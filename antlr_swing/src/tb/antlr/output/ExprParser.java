// $ANTLR 3.5.1 /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g 2021-03-16 00:52:47

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.debug.*;
import java.io.IOException;
import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class ExprParser extends DebugParser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "BITAND", "BITOR", "BITXOR", "BLOCK", 
		"DIV", "ELSE", "ID", "IF", "INT", "LB", "LP", "LSHIFT", "MINUS", "MOD", 
		"MUL", "NL", "PLUS", "PODST", "PRINT", "RB", "RP", "RSHIFT", "THEN", "VAR", 
		"WS"
	};
	public static final int EOF=-1;
	public static final int BITAND=4;
	public static final int BITOR=5;
	public static final int BITXOR=6;
	public static final int BLOCK=7;
	public static final int DIV=8;
	public static final int ELSE=9;
	public static final int ID=10;
	public static final int IF=11;
	public static final int INT=12;
	public static final int LB=13;
	public static final int LP=14;
	public static final int LSHIFT=15;
	public static final int MINUS=16;
	public static final int MOD=17;
	public static final int MUL=18;
	public static final int NL=19;
	public static final int PLUS=20;
	public static final int PODST=21;
	public static final int PRINT=22;
	public static final int RB=23;
	public static final int RP=24;
	public static final int RSHIFT=25;
	public static final int THEN=26;
	public static final int VAR=27;
	public static final int WS=28;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public static final String[] ruleNames = new String[] {
		"invalidRule", "multExpr", "prog", "stat", "lines", "bitxorexpr", "block", 
		"expr", "atom", "addExpr", "bitandexpr", "shiftexpr"
	};

	public static final boolean[] decisionCanBacktrack = new boolean[] {
		false, // invalid decision
		false, false, false, false, false, false, false, false, false, false
	};

 
	public int ruleLevel = 0;
	public int getRuleLevel() { return ruleLevel; }
	public void incRuleLevel() { ruleLevel++; }
	public void decRuleLevel() { ruleLevel--; }
	public ExprParser(TokenStream input) {
		this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
	}
	public ExprParser(TokenStream input, int port, RecognizerSharedState state) {
		super(input, state);
		DebugEventSocketProxy proxy =
			new DebugEventSocketProxy(this,port,adaptor);
		setDebugListener(proxy);
		setTokenStream(new DebugTokenStream(input,proxy));
		try {
			proxy.handshake();
		}
		catch (IOException ioe) {
			reportError(ioe);
		}
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);
		proxy.setTreeAdaptor(adap);
	}

	public ExprParser(TokenStream input, DebugEventListener dbg) {
		super(input, dbg);
		 
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);

	}

	protected boolean evalPredicate(boolean result, String predicate) {
		dbg.semanticPredicate(result, predicate);
		return result;
	}

		protected DebugTreeAdaptor adaptor;
		public void setTreeAdaptor(TreeAdaptor adaptor) {
			this.adaptor = new DebugTreeAdaptor(dbg,adaptor);
		}
		public TreeAdaptor getTreeAdaptor() {
			return adaptor;
		}
	@Override public String[] getTokenNames() { return ExprParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g"; }


	public static class prog_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "prog"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:20:1: prog : lines EOF !;
	public final ExprParser.prog_return prog() throws RecognitionException {
		ExprParser.prog_return retval = new ExprParser.prog_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token EOF2=null;
		ParserRuleReturnScope lines1 =null;

		CommonTree EOF2_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "prog");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(20, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:21:5: ( lines EOF !)
			dbg.enterAlt(1);

			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:21:7: lines EOF !
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(21,7);
			pushFollow(FOLLOW_lines_in_prog58);
			lines1=lines();
			state._fsp--;

			adaptor.addChild(root_0, lines1.getTree());
			dbg.location(21,16);
			EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_prog60); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(22, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "prog");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "prog"


	public static class lines_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "lines"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:24:1: lines : ( stat | block )+ ;
	public final ExprParser.lines_return lines() throws RecognitionException {
		ExprParser.lines_return retval = new ExprParser.lines_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope stat3 =null;
		ParserRuleReturnScope block4 =null;


		try { dbg.enterRule(getGrammarFileName(), "lines");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(24, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:25:5: ( ( stat | block )+ )
			dbg.enterAlt(1);

			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:25:7: ( stat | block )+
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(25,7);
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:25:7: ( stat | block )+
			int cnt1=0;
			try { dbg.enterSubRule(1);

			loop1:
			while (true) {
				int alt1=3;
				try { dbg.enterDecision(1, decisionCanBacktrack[1]);

				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= ID && LA1_0 <= INT)||LA1_0==LP||LA1_0==NL||LA1_0==PRINT||LA1_0==VAR) ) {
					alt1=1;
				}
				else if ( (LA1_0==LB) ) {
					alt1=2;
				}

				} finally {dbg.exitDecision(1);}

				switch (alt1) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:25:8: stat
					{
					dbg.location(25,8);
					pushFollow(FOLLOW_stat_in_lines83);
					stat3=stat();
					state._fsp--;

					adaptor.addChild(root_0, stat3.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:25:15: block
					{
					dbg.location(25,15);
					pushFollow(FOLLOW_block_in_lines87);
					block4=block();
					state._fsp--;

					adaptor.addChild(root_0, block4.getTree());

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					dbg.recognitionException(eee);

					throw eee;
				}
				cnt1++;
			}
			} finally {dbg.exitSubRule(1);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(26, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "lines");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "lines"


	public static class block_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "block"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:28:1: block : LB body= lines RB NL -> ^( BLOCK $body) ;
	public final ExprParser.block_return block() throws RecognitionException {
		ExprParser.block_return retval = new ExprParser.block_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token LB5=null;
		Token RB6=null;
		Token NL7=null;
		ParserRuleReturnScope body =null;

		CommonTree LB5_tree=null;
		CommonTree RB6_tree=null;
		CommonTree NL7_tree=null;
		RewriteRuleTokenStream stream_RB=new RewriteRuleTokenStream(adaptor,"token RB");
		RewriteRuleTokenStream stream_LB=new RewriteRuleTokenStream(adaptor,"token LB");
		RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
		RewriteRuleSubtreeStream stream_lines=new RewriteRuleSubtreeStream(adaptor,"rule lines");

		try { dbg.enterRule(getGrammarFileName(), "block");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(28, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:29:5: ( LB body= lines RB NL -> ^( BLOCK $body) )
			dbg.enterAlt(1);

			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:29:7: LB body= lines RB NL
			{
			dbg.location(29,7);
			LB5=(Token)match(input,LB,FOLLOW_LB_in_block114);  
			stream_LB.add(LB5);
			dbg.location(29,14);
			pushFollow(FOLLOW_lines_in_block118);
			body=lines();
			state._fsp--;

			stream_lines.add(body.getTree());dbg.location(29,21);
			RB6=(Token)match(input,RB,FOLLOW_RB_in_block120);  
			stream_RB.add(RB6);
			dbg.location(29,24);
			NL7=(Token)match(input,NL,FOLLOW_NL_in_block122);  
			stream_NL.add(NL7);

			// AST REWRITE
			// elements: body
			// token labels: 
			// rule labels: body, retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_body=new RewriteRuleSubtreeStream(adaptor,"rule body",body!=null?body.getTree():null);
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 29:27: -> ^( BLOCK $body)
			{
				dbg.location(29,30);
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:29:30: ^( BLOCK $body)
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				dbg.location(29,32);
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, "BLOCK"), root_1);
				dbg.location(29,39);
				adaptor.addChild(root_1, stream_body.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(30, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "block");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "block"


	public static class stat_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "stat"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:32:1: stat : ( expr NL -> expr | VAR ID PODST expr NL -> ^( VAR ID ) ^( PODST ID expr ) | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | PRINT expr NL -> ^( PRINT expr ) | IF cond= expr THEN th= expr ( ELSE el= expr )? NL -> ^( IF $cond $th ( $el)? ) | NL ->);
	public final ExprParser.stat_return stat() throws RecognitionException {
		ExprParser.stat_return retval = new ExprParser.stat_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token NL9=null;
		Token VAR10=null;
		Token ID11=null;
		Token PODST12=null;
		Token NL14=null;
		Token VAR15=null;
		Token ID16=null;
		Token NL17=null;
		Token ID18=null;
		Token PODST19=null;
		Token NL21=null;
		Token PRINT22=null;
		Token NL24=null;
		Token IF25=null;
		Token THEN26=null;
		Token ELSE27=null;
		Token NL28=null;
		Token NL29=null;
		ParserRuleReturnScope cond =null;
		ParserRuleReturnScope th =null;
		ParserRuleReturnScope el =null;
		ParserRuleReturnScope expr8 =null;
		ParserRuleReturnScope expr13 =null;
		ParserRuleReturnScope expr20 =null;
		ParserRuleReturnScope expr23 =null;

		CommonTree NL9_tree=null;
		CommonTree VAR10_tree=null;
		CommonTree ID11_tree=null;
		CommonTree PODST12_tree=null;
		CommonTree NL14_tree=null;
		CommonTree VAR15_tree=null;
		CommonTree ID16_tree=null;
		CommonTree NL17_tree=null;
		CommonTree ID18_tree=null;
		CommonTree PODST19_tree=null;
		CommonTree NL21_tree=null;
		CommonTree PRINT22_tree=null;
		CommonTree NL24_tree=null;
		CommonTree IF25_tree=null;
		CommonTree THEN26_tree=null;
		CommonTree ELSE27_tree=null;
		CommonTree NL28_tree=null;
		CommonTree NL29_tree=null;
		RewriteRuleTokenStream stream_PRINT=new RewriteRuleTokenStream(adaptor,"token PRINT");
		RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
		RewriteRuleTokenStream stream_ELSE=new RewriteRuleTokenStream(adaptor,"token ELSE");
		RewriteRuleTokenStream stream_PODST=new RewriteRuleTokenStream(adaptor,"token PODST");
		RewriteRuleTokenStream stream_THEN=new RewriteRuleTokenStream(adaptor,"token THEN");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_IF=new RewriteRuleTokenStream(adaptor,"token IF");
		RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try { dbg.enterRule(getGrammarFileName(), "stat");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(32, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:33:5: ( expr NL -> expr | VAR ID PODST expr NL -> ^( VAR ID ) ^( PODST ID expr ) | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | PRINT expr NL -> ^( PRINT expr ) | IF cond= expr THEN th= expr ( ELSE el= expr )? NL -> ^( IF $cond $th ( $el)? ) | NL ->)
			int alt3=7;
			try { dbg.enterDecision(3, decisionCanBacktrack[3]);

			switch ( input.LA(1) ) {
			case INT:
			case LP:
				{
				alt3=1;
				}
				break;
			case ID:
				{
				int LA3_2 = input.LA(2);
				if ( (LA3_2==PODST) ) {
					alt3=4;
				}
				else if ( ((LA3_2 >= BITAND && LA3_2 <= BITXOR)||LA3_2==DIV||(LA3_2 >= LSHIFT && LA3_2 <= PLUS)||LA3_2==RSHIFT) ) {
					alt3=1;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 3, 2, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case VAR:
				{
				int LA3_3 = input.LA(2);
				if ( (LA3_3==ID) ) {
					int LA3_8 = input.LA(3);
					if ( (LA3_8==PODST) ) {
						alt3=2;
					}
					else if ( (LA3_8==NL) ) {
						alt3=3;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 3, 8, input);
							dbg.recognitionException(nvae);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 3, 3, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case PRINT:
				{
				alt3=5;
				}
				break;
			case IF:
				{
				alt3=6;
				}
				break;
			case NL:
				{
				alt3=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(3);}

			switch (alt3) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:33:7: expr NL
					{
					dbg.location(33,7);
					pushFollow(FOLLOW_expr_in_stat149);
					expr8=expr();
					state._fsp--;

					stream_expr.add(expr8.getTree());dbg.location(33,12);
					NL9=(Token)match(input,NL,FOLLOW_NL_in_stat151);  
					stream_NL.add(NL9);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 33:15: -> expr
					{
						dbg.location(33,18);
						adaptor.addChild(root_0, stream_expr.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:34:7: VAR ID PODST expr NL
					{
					dbg.location(34,7);
					VAR10=(Token)match(input,VAR,FOLLOW_VAR_in_stat163);  
					stream_VAR.add(VAR10);
					dbg.location(34,11);
					ID11=(Token)match(input,ID,FOLLOW_ID_in_stat165);  
					stream_ID.add(ID11);
					dbg.location(34,14);
					PODST12=(Token)match(input,PODST,FOLLOW_PODST_in_stat167);  
					stream_PODST.add(PODST12);
					dbg.location(34,20);
					pushFollow(FOLLOW_expr_in_stat169);
					expr13=expr();
					state._fsp--;

					stream_expr.add(expr13.getTree());dbg.location(34,25);
					NL14=(Token)match(input,NL,FOLLOW_NL_in_stat171);  
					stream_NL.add(NL14);

					// AST REWRITE
					// elements: PODST, VAR, expr, ID, ID
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 34:28: -> ^( VAR ID ) ^( PODST ID expr )
					{
						dbg.location(34,31);
						// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:34:31: ^( VAR ID )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(34,33);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_VAR.nextNode(), root_1);
						dbg.location(34,37);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}
						dbg.location(34,41);
						// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:34:41: ^( PODST ID expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(34,43);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
						dbg.location(34,49);
						adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(34,52);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:35:7: VAR ID NL
					{
					dbg.location(35,7);
					VAR15=(Token)match(input,VAR,FOLLOW_VAR_in_stat195);  
					stream_VAR.add(VAR15);
					dbg.location(35,11);
					ID16=(Token)match(input,ID,FOLLOW_ID_in_stat197);  
					stream_ID.add(ID16);
					dbg.location(35,14);
					NL17=(Token)match(input,NL,FOLLOW_NL_in_stat199);  
					stream_NL.add(NL17);

					// AST REWRITE
					// elements: ID, VAR
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 35:17: -> ^( VAR ID )
					{
						dbg.location(35,20);
						// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:35:20: ^( VAR ID )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(35,22);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_VAR.nextNode(), root_1);
						dbg.location(35,26);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:36:7: ID PODST expr NL
					{
					dbg.location(36,7);
					ID18=(Token)match(input,ID,FOLLOW_ID_in_stat215);  
					stream_ID.add(ID18);
					dbg.location(36,10);
					PODST19=(Token)match(input,PODST,FOLLOW_PODST_in_stat217);  
					stream_PODST.add(PODST19);
					dbg.location(36,16);
					pushFollow(FOLLOW_expr_in_stat219);
					expr20=expr();
					state._fsp--;

					stream_expr.add(expr20.getTree());dbg.location(36,21);
					NL21=(Token)match(input,NL,FOLLOW_NL_in_stat221);  
					stream_NL.add(NL21);

					// AST REWRITE
					// elements: PODST, expr, ID
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 36:24: -> ^( PODST ID expr )
					{
						dbg.location(36,27);
						// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:36:27: ^( PODST ID expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(36,29);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
						dbg.location(36,35);
						adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(36,38);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:37:7: PRINT expr NL
					{
					dbg.location(37,7);
					PRINT22=(Token)match(input,PRINT,FOLLOW_PRINT_in_stat239);  
					stream_PRINT.add(PRINT22);
					dbg.location(37,13);
					pushFollow(FOLLOW_expr_in_stat241);
					expr23=expr();
					state._fsp--;

					stream_expr.add(expr23.getTree());dbg.location(37,18);
					NL24=(Token)match(input,NL,FOLLOW_NL_in_stat243);  
					stream_NL.add(NL24);

					// AST REWRITE
					// elements: expr, PRINT
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 37:21: -> ^( PRINT expr )
					{
						dbg.location(37,24);
						// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:37:24: ^( PRINT expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(37,26);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PRINT.nextNode(), root_1);
						dbg.location(37,32);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:38:7: IF cond= expr THEN th= expr ( ELSE el= expr )? NL
					{
					dbg.location(38,7);
					IF25=(Token)match(input,IF,FOLLOW_IF_in_stat259);  
					stream_IF.add(IF25);
					dbg.location(38,14);
					pushFollow(FOLLOW_expr_in_stat263);
					cond=expr();
					state._fsp--;

					stream_expr.add(cond.getTree());dbg.location(38,20);
					THEN26=(Token)match(input,THEN,FOLLOW_THEN_in_stat265);  
					stream_THEN.add(THEN26);
					dbg.location(38,27);
					pushFollow(FOLLOW_expr_in_stat269);
					th=expr();
					state._fsp--;

					stream_expr.add(th.getTree());dbg.location(38,33);
					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:38:33: ( ELSE el= expr )?
					int alt2=2;
					try { dbg.enterSubRule(2);
					try { dbg.enterDecision(2, decisionCanBacktrack[2]);

					int LA2_0 = input.LA(1);
					if ( (LA2_0==ELSE) ) {
						alt2=1;
					}
					} finally {dbg.exitDecision(2);}

					switch (alt2) {
						case 1 :
							dbg.enterAlt(1);

							// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:38:34: ELSE el= expr
							{
							dbg.location(38,34);
							ELSE27=(Token)match(input,ELSE,FOLLOW_ELSE_in_stat272);  
							stream_ELSE.add(ELSE27);
							dbg.location(38,41);
							pushFollow(FOLLOW_expr_in_stat276);
							el=expr();
							state._fsp--;

							stream_expr.add(el.getTree());
							}
							break;

					}
					} finally {dbg.exitSubRule(2);}
					dbg.location(38,49);
					NL28=(Token)match(input,NL,FOLLOW_NL_in_stat280);  
					stream_NL.add(NL28);

					// AST REWRITE
					// elements: cond, el, th, IF
					// token labels: 
					// rule labels: th, el, cond, retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_th=new RewriteRuleSubtreeStream(adaptor,"rule th",th!=null?th.getTree():null);
					RewriteRuleSubtreeStream stream_el=new RewriteRuleSubtreeStream(adaptor,"rule el",el!=null?el.getTree():null);
					RewriteRuleSubtreeStream stream_cond=new RewriteRuleSubtreeStream(adaptor,"rule cond",cond!=null?cond.getTree():null);
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 38:52: -> ^( IF $cond $th ( $el)? )
					{
						dbg.location(38,55);
						// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:38:55: ^( IF $cond $th ( $el)? )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(38,57);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_IF.nextNode(), root_1);
						dbg.location(38,61);
						adaptor.addChild(root_1, stream_cond.nextTree());dbg.location(38,67);
						adaptor.addChild(root_1, stream_th.nextTree());dbg.location(38,71);
						// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:38:71: ( $el)?
						if ( stream_el.hasNext() ) {
							dbg.location(38,71);
							adaptor.addChild(root_1, stream_el.nextTree());
						}
						stream_el.reset();

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 7 :
					dbg.enterAlt(7);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:39:7: NL
					{
					dbg.location(39,7);
					NL29=(Token)match(input,NL,FOLLOW_NL_in_stat304);  
					stream_NL.add(NL29);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 39:10: ->
					{
						dbg.location(40,5);
						root_0 = null;
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(40, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "stat");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "stat"


	public static class expr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:42:1: expr : bitxorexpr ( BITOR ^ bitxorexpr )* ;
	public final ExprParser.expr_return expr() throws RecognitionException {
		ExprParser.expr_return retval = new ExprParser.expr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token BITOR31=null;
		ParserRuleReturnScope bitxorexpr30 =null;
		ParserRuleReturnScope bitxorexpr32 =null;

		CommonTree BITOR31_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "expr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(42, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:43:3: ( bitxorexpr ( BITOR ^ bitxorexpr )* )
			dbg.enterAlt(1);

			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:43:5: bitxorexpr ( BITOR ^ bitxorexpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(43,5);
			pushFollow(FOLLOW_bitxorexpr_in_expr325);
			bitxorexpr30=bitxorexpr();
			state._fsp--;

			adaptor.addChild(root_0, bitxorexpr30.getTree());
			dbg.location(44,5);
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:44:5: ( BITOR ^ bitxorexpr )*
			try { dbg.enterSubRule(4);

			loop4:
			while (true) {
				int alt4=2;
				try { dbg.enterDecision(4, decisionCanBacktrack[4]);

				int LA4_0 = input.LA(1);
				if ( (LA4_0==BITOR) ) {
					alt4=1;
				}

				} finally {dbg.exitDecision(4);}

				switch (alt4) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:44:7: BITOR ^ bitxorexpr
					{
					dbg.location(44,12);
					BITOR31=(Token)match(input,BITOR,FOLLOW_BITOR_in_expr333); 
					BITOR31_tree = (CommonTree)adaptor.create(BITOR31);
					root_0 = (CommonTree)adaptor.becomeRoot(BITOR31_tree, root_0);
					dbg.location(44,14);
					pushFollow(FOLLOW_bitxorexpr_in_expr336);
					bitxorexpr32=bitxorexpr();
					state._fsp--;

					adaptor.addChild(root_0, bitxorexpr32.getTree());

					}
					break;

				default :
					break loop4;
				}
			}
			} finally {dbg.exitSubRule(4);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(46, 2);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "expr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "expr"


	public static class bitxorexpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "bitxorexpr"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:48:1: bitxorexpr : bitandexpr ( BITXOR ^ bitandexpr )* ;
	public final ExprParser.bitxorexpr_return bitxorexpr() throws RecognitionException {
		ExprParser.bitxorexpr_return retval = new ExprParser.bitxorexpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token BITXOR34=null;
		ParserRuleReturnScope bitandexpr33 =null;
		ParserRuleReturnScope bitandexpr35 =null;

		CommonTree BITXOR34_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "bitxorexpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(48, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:49:3: ( bitandexpr ( BITXOR ^ bitandexpr )* )
			dbg.enterAlt(1);

			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:49:5: bitandexpr ( BITXOR ^ bitandexpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(49,5);
			pushFollow(FOLLOW_bitandexpr_in_bitxorexpr356);
			bitandexpr33=bitandexpr();
			state._fsp--;

			adaptor.addChild(root_0, bitandexpr33.getTree());
			dbg.location(50,5);
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:50:5: ( BITXOR ^ bitandexpr )*
			try { dbg.enterSubRule(5);

			loop5:
			while (true) {
				int alt5=2;
				try { dbg.enterDecision(5, decisionCanBacktrack[5]);

				int LA5_0 = input.LA(1);
				if ( (LA5_0==BITXOR) ) {
					alt5=1;
				}

				} finally {dbg.exitDecision(5);}

				switch (alt5) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:50:7: BITXOR ^ bitandexpr
					{
					dbg.location(50,13);
					BITXOR34=(Token)match(input,BITXOR,FOLLOW_BITXOR_in_bitxorexpr364); 
					BITXOR34_tree = (CommonTree)adaptor.create(BITXOR34);
					root_0 = (CommonTree)adaptor.becomeRoot(BITXOR34_tree, root_0);
					dbg.location(50,15);
					pushFollow(FOLLOW_bitandexpr_in_bitxorexpr367);
					bitandexpr35=bitandexpr();
					state._fsp--;

					adaptor.addChild(root_0, bitandexpr35.getTree());

					}
					break;

				default :
					break loop5;
				}
			}
			} finally {dbg.exitSubRule(5);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(52, 2);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "bitxorexpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "bitxorexpr"


	public static class bitandexpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "bitandexpr"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:54:1: bitandexpr : shiftexpr ( BITAND ^ shiftexpr )* ;
	public final ExprParser.bitandexpr_return bitandexpr() throws RecognitionException {
		ExprParser.bitandexpr_return retval = new ExprParser.bitandexpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token BITAND37=null;
		ParserRuleReturnScope shiftexpr36 =null;
		ParserRuleReturnScope shiftexpr38 =null;

		CommonTree BITAND37_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "bitandexpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(54, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:55:3: ( shiftexpr ( BITAND ^ shiftexpr )* )
			dbg.enterAlt(1);

			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:55:5: shiftexpr ( BITAND ^ shiftexpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(55,5);
			pushFollow(FOLLOW_shiftexpr_in_bitandexpr389);
			shiftexpr36=shiftexpr();
			state._fsp--;

			adaptor.addChild(root_0, shiftexpr36.getTree());
			dbg.location(56,5);
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:56:5: ( BITAND ^ shiftexpr )*
			try { dbg.enterSubRule(6);

			loop6:
			while (true) {
				int alt6=2;
				try { dbg.enterDecision(6, decisionCanBacktrack[6]);

				int LA6_0 = input.LA(1);
				if ( (LA6_0==BITAND) ) {
					alt6=1;
				}

				} finally {dbg.exitDecision(6);}

				switch (alt6) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:56:7: BITAND ^ shiftexpr
					{
					dbg.location(56,13);
					BITAND37=(Token)match(input,BITAND,FOLLOW_BITAND_in_bitandexpr397); 
					BITAND37_tree = (CommonTree)adaptor.create(BITAND37);
					root_0 = (CommonTree)adaptor.becomeRoot(BITAND37_tree, root_0);
					dbg.location(56,15);
					pushFollow(FOLLOW_shiftexpr_in_bitandexpr400);
					shiftexpr38=shiftexpr();
					state._fsp--;

					adaptor.addChild(root_0, shiftexpr38.getTree());

					}
					break;

				default :
					break loop6;
				}
			}
			} finally {dbg.exitSubRule(6);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(58, 2);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "bitandexpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "bitandexpr"


	public static class shiftexpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "shiftexpr"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:60:1: shiftexpr : addExpr ( LSHIFT ^ addExpr | RSHIFT ^ addExpr )* ;
	public final ExprParser.shiftexpr_return shiftexpr() throws RecognitionException {
		ExprParser.shiftexpr_return retval = new ExprParser.shiftexpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token LSHIFT40=null;
		Token RSHIFT42=null;
		ParserRuleReturnScope addExpr39 =null;
		ParserRuleReturnScope addExpr41 =null;
		ParserRuleReturnScope addExpr43 =null;

		CommonTree LSHIFT40_tree=null;
		CommonTree RSHIFT42_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "shiftexpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(60, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:61:3: ( addExpr ( LSHIFT ^ addExpr | RSHIFT ^ addExpr )* )
			dbg.enterAlt(1);

			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:61:5: addExpr ( LSHIFT ^ addExpr | RSHIFT ^ addExpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(61,5);
			pushFollow(FOLLOW_addExpr_in_shiftexpr420);
			addExpr39=addExpr();
			state._fsp--;

			adaptor.addChild(root_0, addExpr39.getTree());
			dbg.location(62,5);
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:62:5: ( LSHIFT ^ addExpr | RSHIFT ^ addExpr )*
			try { dbg.enterSubRule(7);

			loop7:
			while (true) {
				int alt7=3;
				try { dbg.enterDecision(7, decisionCanBacktrack[7]);

				int LA7_0 = input.LA(1);
				if ( (LA7_0==LSHIFT) ) {
					alt7=1;
				}
				else if ( (LA7_0==RSHIFT) ) {
					alt7=2;
				}

				} finally {dbg.exitDecision(7);}

				switch (alt7) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:62:7: LSHIFT ^ addExpr
					{
					dbg.location(62,13);
					LSHIFT40=(Token)match(input,LSHIFT,FOLLOW_LSHIFT_in_shiftexpr428); 
					LSHIFT40_tree = (CommonTree)adaptor.create(LSHIFT40);
					root_0 = (CommonTree)adaptor.becomeRoot(LSHIFT40_tree, root_0);
					dbg.location(62,15);
					pushFollow(FOLLOW_addExpr_in_shiftexpr431);
					addExpr41=addExpr();
					state._fsp--;

					adaptor.addChild(root_0, addExpr41.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:63:7: RSHIFT ^ addExpr
					{
					dbg.location(63,13);
					RSHIFT42=(Token)match(input,RSHIFT,FOLLOW_RSHIFT_in_shiftexpr439); 
					RSHIFT42_tree = (CommonTree)adaptor.create(RSHIFT42);
					root_0 = (CommonTree)adaptor.becomeRoot(RSHIFT42_tree, root_0);
					dbg.location(63,15);
					pushFollow(FOLLOW_addExpr_in_shiftexpr442);
					addExpr43=addExpr();
					state._fsp--;

					adaptor.addChild(root_0, addExpr43.getTree());

					}
					break;

				default :
					break loop7;
				}
			}
			} finally {dbg.exitSubRule(7);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(65, 2);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "shiftexpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "shiftexpr"


	public static class addExpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "addExpr"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:67:1: addExpr : multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* ;
	public final ExprParser.addExpr_return addExpr() throws RecognitionException {
		ExprParser.addExpr_return retval = new ExprParser.addExpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token PLUS45=null;
		Token MINUS47=null;
		ParserRuleReturnScope multExpr44 =null;
		ParserRuleReturnScope multExpr46 =null;
		ParserRuleReturnScope multExpr48 =null;

		CommonTree PLUS45_tree=null;
		CommonTree MINUS47_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "addExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(67, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:68:5: ( multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* )
			dbg.enterAlt(1);

			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:68:7: multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(68,7);
			pushFollow(FOLLOW_multExpr_in_addExpr465);
			multExpr44=multExpr();
			state._fsp--;

			adaptor.addChild(root_0, multExpr44.getTree());
			dbg.location(69,7);
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:69:7: ( PLUS ^ multExpr | MINUS ^ multExpr )*
			try { dbg.enterSubRule(8);

			loop8:
			while (true) {
				int alt8=3;
				try { dbg.enterDecision(8, decisionCanBacktrack[8]);

				int LA8_0 = input.LA(1);
				if ( (LA8_0==PLUS) ) {
					alt8=1;
				}
				else if ( (LA8_0==MINUS) ) {
					alt8=2;
				}

				} finally {dbg.exitDecision(8);}

				switch (alt8) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:69:9: PLUS ^ multExpr
					{
					dbg.location(69,13);
					PLUS45=(Token)match(input,PLUS,FOLLOW_PLUS_in_addExpr475); 
					PLUS45_tree = (CommonTree)adaptor.create(PLUS45);
					root_0 = (CommonTree)adaptor.becomeRoot(PLUS45_tree, root_0);
					dbg.location(69,15);
					pushFollow(FOLLOW_multExpr_in_addExpr478);
					multExpr46=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr46.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:70:9: MINUS ^ multExpr
					{
					dbg.location(70,14);
					MINUS47=(Token)match(input,MINUS,FOLLOW_MINUS_in_addExpr488); 
					MINUS47_tree = (CommonTree)adaptor.create(MINUS47);
					root_0 = (CommonTree)adaptor.becomeRoot(MINUS47_tree, root_0);
					dbg.location(70,16);
					pushFollow(FOLLOW_multExpr_in_addExpr491);
					multExpr48=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr48.getTree());

					}
					break;

				default :
					break loop8;
				}
			}
			} finally {dbg.exitSubRule(8);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(72, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "addExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "addExpr"


	public static class multExpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "multExpr"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:74:1: multExpr : atom ( MUL ^ atom | DIV ^ atom | MOD ^ atom )* ;
	public final ExprParser.multExpr_return multExpr() throws RecognitionException {
		ExprParser.multExpr_return retval = new ExprParser.multExpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token MUL50=null;
		Token DIV52=null;
		Token MOD54=null;
		ParserRuleReturnScope atom49 =null;
		ParserRuleReturnScope atom51 =null;
		ParserRuleReturnScope atom53 =null;
		ParserRuleReturnScope atom55 =null;

		CommonTree MUL50_tree=null;
		CommonTree DIV52_tree=null;
		CommonTree MOD54_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "multExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(74, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:75:5: ( atom ( MUL ^ atom | DIV ^ atom | MOD ^ atom )* )
			dbg.enterAlt(1);

			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:75:7: atom ( MUL ^ atom | DIV ^ atom | MOD ^ atom )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(75,7);
			pushFollow(FOLLOW_atom_in_multExpr517);
			atom49=atom();
			state._fsp--;

			adaptor.addChild(root_0, atom49.getTree());
			dbg.location(76,7);
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:76:7: ( MUL ^ atom | DIV ^ atom | MOD ^ atom )*
			try { dbg.enterSubRule(9);

			loop9:
			while (true) {
				int alt9=4;
				try { dbg.enterDecision(9, decisionCanBacktrack[9]);

				switch ( input.LA(1) ) {
				case MUL:
					{
					alt9=1;
					}
					break;
				case DIV:
					{
					alt9=2;
					}
					break;
				case MOD:
					{
					alt9=3;
					}
					break;
				}
				} finally {dbg.exitDecision(9);}

				switch (alt9) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:76:9: MUL ^ atom
					{
					dbg.location(76,12);
					MUL50=(Token)match(input,MUL,FOLLOW_MUL_in_multExpr527); 
					MUL50_tree = (CommonTree)adaptor.create(MUL50);
					root_0 = (CommonTree)adaptor.becomeRoot(MUL50_tree, root_0);
					dbg.location(76,14);
					pushFollow(FOLLOW_atom_in_multExpr530);
					atom51=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom51.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:77:9: DIV ^ atom
					{
					dbg.location(77,12);
					DIV52=(Token)match(input,DIV,FOLLOW_DIV_in_multExpr540); 
					DIV52_tree = (CommonTree)adaptor.create(DIV52);
					root_0 = (CommonTree)adaptor.becomeRoot(DIV52_tree, root_0);
					dbg.location(77,14);
					pushFollow(FOLLOW_atom_in_multExpr543);
					atom53=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom53.getTree());

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:78:9: MOD ^ atom
					{
					dbg.location(78,12);
					MOD54=(Token)match(input,MOD,FOLLOW_MOD_in_multExpr553); 
					MOD54_tree = (CommonTree)adaptor.create(MOD54);
					root_0 = (CommonTree)adaptor.becomeRoot(MOD54_tree, root_0);
					dbg.location(78,14);
					pushFollow(FOLLOW_atom_in_multExpr556);
					atom55=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom55.getTree());

					}
					break;

				default :
					break loop9;
				}
			}
			} finally {dbg.exitSubRule(9);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(80, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "multExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "multExpr"


	public static class atom_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "atom"
	// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:82:1: atom : ( INT | ID | LP ! expr RP !);
	public final ExprParser.atom_return atom() throws RecognitionException {
		ExprParser.atom_return retval = new ExprParser.atom_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token INT56=null;
		Token ID57=null;
		Token LP58=null;
		Token RP60=null;
		ParserRuleReturnScope expr59 =null;

		CommonTree INT56_tree=null;
		CommonTree ID57_tree=null;
		CommonTree LP58_tree=null;
		CommonTree RP60_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "atom");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(82, 0);

		try {
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:83:5: ( INT | ID | LP ! expr RP !)
			int alt10=3;
			try { dbg.enterDecision(10, decisionCanBacktrack[10]);

			switch ( input.LA(1) ) {
			case INT:
				{
				alt10=1;
				}
				break;
			case ID:
				{
				alt10=2;
				}
				break;
			case LP:
				{
				alt10=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 10, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(10);}

			switch (alt10) {
				case 1 :
					dbg.enterAlt(1);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:83:7: INT
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(83,7);
					INT56=(Token)match(input,INT,FOLLOW_INT_in_atom582); 
					INT56_tree = (CommonTree)adaptor.create(INT56);
					adaptor.addChild(root_0, INT56_tree);

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:84:7: ID
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(84,7);
					ID57=(Token)match(input,ID,FOLLOW_ID_in_atom590); 
					ID57_tree = (CommonTree)adaptor.create(ID57);
					adaptor.addChild(root_0, ID57_tree);

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:85:7: LP ! expr RP !
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(85,9);
					LP58=(Token)match(input,LP,FOLLOW_LP_in_atom598); dbg.location(85,11);
					pushFollow(FOLLOW_expr_in_atom601);
					expr59=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr59.getTree());
					dbg.location(85,18);
					RP60=(Token)match(input,RP,FOLLOW_RP_in_atom603); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(86, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "atom");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "atom"

	// Delegated rules



	public static final BitSet FOLLOW_lines_in_prog58 = new BitSet(new long[]{0x0000000000000000L});
	public static final BitSet FOLLOW_EOF_in_prog60 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_stat_in_lines83 = new BitSet(new long[]{0x0000000008487C02L});
	public static final BitSet FOLLOW_block_in_lines87 = new BitSet(new long[]{0x0000000008487C02L});
	public static final BitSet FOLLOW_LB_in_block114 = new BitSet(new long[]{0x0000000008487C00L});
	public static final BitSet FOLLOW_lines_in_block118 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_RB_in_block120 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_NL_in_block122 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_stat149 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_NL_in_stat151 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_stat163 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_ID_in_stat165 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_PODST_in_stat167 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_expr_in_stat169 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_NL_in_stat171 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_stat195 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_ID_in_stat197 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_NL_in_stat199 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_stat215 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_PODST_in_stat217 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_expr_in_stat219 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_NL_in_stat221 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_stat239 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_expr_in_stat241 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_NL_in_stat243 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_stat259 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_expr_in_stat263 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_THEN_in_stat265 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_expr_in_stat269 = new BitSet(new long[]{0x0000000000080200L});
	public static final BitSet FOLLOW_ELSE_in_stat272 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_expr_in_stat276 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_NL_in_stat280 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NL_in_stat304 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_bitxorexpr_in_expr325 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_BITOR_in_expr333 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_bitxorexpr_in_expr336 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_bitandexpr_in_bitxorexpr356 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_BITXOR_in_bitxorexpr364 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_bitandexpr_in_bitxorexpr367 = new BitSet(new long[]{0x0000000000000042L});
	public static final BitSet FOLLOW_shiftexpr_in_bitandexpr389 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_BITAND_in_bitandexpr397 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_shiftexpr_in_bitandexpr400 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_addExpr_in_shiftexpr420 = new BitSet(new long[]{0x0000000002008002L});
	public static final BitSet FOLLOW_LSHIFT_in_shiftexpr428 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_addExpr_in_shiftexpr431 = new BitSet(new long[]{0x0000000002008002L});
	public static final BitSet FOLLOW_RSHIFT_in_shiftexpr439 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_addExpr_in_shiftexpr442 = new BitSet(new long[]{0x0000000002008002L});
	public static final BitSet FOLLOW_multExpr_in_addExpr465 = new BitSet(new long[]{0x0000000000110002L});
	public static final BitSet FOLLOW_PLUS_in_addExpr475 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_multExpr_in_addExpr478 = new BitSet(new long[]{0x0000000000110002L});
	public static final BitSet FOLLOW_MINUS_in_addExpr488 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_multExpr_in_addExpr491 = new BitSet(new long[]{0x0000000000110002L});
	public static final BitSet FOLLOW_atom_in_multExpr517 = new BitSet(new long[]{0x0000000000060102L});
	public static final BitSet FOLLOW_MUL_in_multExpr527 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_atom_in_multExpr530 = new BitSet(new long[]{0x0000000000060102L});
	public static final BitSet FOLLOW_DIV_in_multExpr540 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_atom_in_multExpr543 = new BitSet(new long[]{0x0000000000060102L});
	public static final BitSet FOLLOW_MOD_in_multExpr553 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_atom_in_multExpr556 = new BitSet(new long[]{0x0000000000060102L});
	public static final BitSet FOLLOW_INT_in_atom582 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_atom590 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LP_in_atom598 = new BitSet(new long[]{0x0000000000005400L});
	public static final BitSet FOLLOW_expr_in_atom601 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_RP_in_atom603 = new BitSet(new long[]{0x0000000000000002L});
}
