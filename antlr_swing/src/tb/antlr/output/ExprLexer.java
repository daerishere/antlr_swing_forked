// $ANTLR 3.5.1 /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g 2021-03-16 00:52:47

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ExprLexer extends Lexer {
	public static final int EOF=-1;
	public static final int BITAND=4;
	public static final int BITOR=5;
	public static final int BITXOR=6;
	public static final int BLOCK=7;
	public static final int DIV=8;
	public static final int ELSE=9;
	public static final int ID=10;
	public static final int IF=11;
	public static final int INT=12;
	public static final int LB=13;
	public static final int LP=14;
	public static final int LSHIFT=15;
	public static final int MINUS=16;
	public static final int MOD=17;
	public static final int MUL=18;
	public static final int NL=19;
	public static final int PLUS=20;
	public static final int PODST=21;
	public static final int PRINT=22;
	public static final int RB=23;
	public static final int RP=24;
	public static final int RSHIFT=25;
	public static final int THEN=26;
	public static final int VAR=27;
	public static final int WS=28;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ExprLexer() {} 
	public ExprLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ExprLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g"; }

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:88:5: ( 'var' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:88:6: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:90:7: ( 'print' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:90:9: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:92:4: ( 'if' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:92:6: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "THEN"
	public final void mTHEN() throws RecognitionException {
		try {
			int _type = THEN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:94:6: ( 'then' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:94:8: 'then'
			{
			match("then"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "THEN"

	// $ANTLR start "ELSE"
	public final void mELSE() throws RecognitionException {
		try {
			int _type = ELSE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:96:6: ( 'else' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:96:8: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ELSE"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:98:4: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:98:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:98:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:100:5: ( ( '0' .. '9' )+ )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:100:7: ( '0' .. '9' )+
			{
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:100:7: ( '0' .. '9' )+
			int cnt2=0;
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt2 >= 1 ) break loop2;
					EarlyExitException eee = new EarlyExitException(2, input);
					throw eee;
				}
				cnt2++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "NL"
	public final void mNL() throws RecognitionException {
		try {
			int _type = NL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:102:4: ( ( '\\r' )? '\\n' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:102:6: ( '\\r' )? '\\n'
			{
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:102:6: ( '\\r' )?
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( (LA3_0=='\r') ) {
				alt3=1;
			}
			switch (alt3) {
				case 1 :
					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:102:6: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NL"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:104:4: ( ( ' ' | '\\t' )+ )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:104:6: ( ' ' | '\\t' )+
			{
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:104:6: ( ' ' | '\\t' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0=='\t'||LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:
					{
					if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			_channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "LP"
	public final void mLP() throws RecognitionException {
		try {
			int _type = LP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:108:2: ( '(' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:108:4: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LP"

	// $ANTLR start "RP"
	public final void mRP() throws RecognitionException {
		try {
			int _type = RP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:112:2: ( ')' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:112:4: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RP"

	// $ANTLR start "LB"
	public final void mLB() throws RecognitionException {
		try {
			int _type = LB;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:116:3: ( '{' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:116:5: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LB"

	// $ANTLR start "RB"
	public final void mRB() throws RecognitionException {
		try {
			int _type = RB;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:120:3: ( '}' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:120:5: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RB"

	// $ANTLR start "PODST"
	public final void mPODST() throws RecognitionException {
		try {
			int _type = PODST;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:124:2: ( '=' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:124:4: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PODST"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:128:2: ( '+' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:128:4: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:132:2: ( '-' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:132:4: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MUL"
	public final void mMUL() throws RecognitionException {
		try {
			int _type = MUL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:136:2: ( '*' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:136:4: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MUL"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:140:2: ( '/' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:140:4: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "MOD"
	public final void mMOD() throws RecognitionException {
		try {
			int _type = MOD;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:144:3: ( '%' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:144:5: '%'
			{
			match('%'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MOD"

	// $ANTLR start "LSHIFT"
	public final void mLSHIFT() throws RecognitionException {
		try {
			int _type = LSHIFT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:148:3: ( '<<' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:148:5: '<<'
			{
			match("<<"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LSHIFT"

	// $ANTLR start "RSHIFT"
	public final void mRSHIFT() throws RecognitionException {
		try {
			int _type = RSHIFT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:152:3: ( '>>' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:152:5: '>>'
			{
			match(">>"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RSHIFT"

	// $ANTLR start "BITAND"
	public final void mBITAND() throws RecognitionException {
		try {
			int _type = BITAND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:156:3: ( '&' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:156:5: '&'
			{
			match('&'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BITAND"

	// $ANTLR start "BITXOR"
	public final void mBITXOR() throws RecognitionException {
		try {
			int _type = BITXOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:160:3: ( '^' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:160:5: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BITXOR"

	// $ANTLR start "BITOR"
	public final void mBITOR() throws RecognitionException {
		try {
			int _type = BITOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:164:3: ( '|' )
			// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:164:5: '|'
			{
			match('|'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BITOR"

	@Override
	public void mTokens() throws RecognitionException {
		// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:8: ( VAR | PRINT | IF | THEN | ELSE | ID | INT | NL | WS | LP | RP | LB | RB | PODST | PLUS | MINUS | MUL | DIV | MOD | LSHIFT | RSHIFT | BITAND | BITXOR | BITOR )
		int alt5=24;
		switch ( input.LA(1) ) {
		case 'v':
			{
			int LA5_1 = input.LA(2);
			if ( (LA5_1=='a') ) {
				int LA5_25 = input.LA(3);
				if ( (LA5_25=='r') ) {
					int LA5_30 = input.LA(4);
					if ( ((LA5_30 >= '0' && LA5_30 <= '9')||(LA5_30 >= 'A' && LA5_30 <= 'Z')||LA5_30=='_'||(LA5_30 >= 'a' && LA5_30 <= 'z')) ) {
						alt5=6;
					}

					else {
						alt5=1;
					}

				}

				else {
					alt5=6;
				}

			}

			else {
				alt5=6;
			}

			}
			break;
		case 'p':
			{
			int LA5_2 = input.LA(2);
			if ( (LA5_2=='r') ) {
				int LA5_26 = input.LA(3);
				if ( (LA5_26=='i') ) {
					int LA5_31 = input.LA(4);
					if ( (LA5_31=='n') ) {
						int LA5_36 = input.LA(5);
						if ( (LA5_36=='t') ) {
							int LA5_39 = input.LA(6);
							if ( ((LA5_39 >= '0' && LA5_39 <= '9')||(LA5_39 >= 'A' && LA5_39 <= 'Z')||LA5_39=='_'||(LA5_39 >= 'a' && LA5_39 <= 'z')) ) {
								alt5=6;
							}

							else {
								alt5=2;
							}

						}

						else {
							alt5=6;
						}

					}

					else {
						alt5=6;
					}

				}

				else {
					alt5=6;
				}

			}

			else {
				alt5=6;
			}

			}
			break;
		case 'i':
			{
			int LA5_3 = input.LA(2);
			if ( (LA5_3=='f') ) {
				int LA5_27 = input.LA(3);
				if ( ((LA5_27 >= '0' && LA5_27 <= '9')||(LA5_27 >= 'A' && LA5_27 <= 'Z')||LA5_27=='_'||(LA5_27 >= 'a' && LA5_27 <= 'z')) ) {
					alt5=6;
				}

				else {
					alt5=3;
				}

			}

			else {
				alt5=6;
			}

			}
			break;
		case 't':
			{
			int LA5_4 = input.LA(2);
			if ( (LA5_4=='h') ) {
				int LA5_28 = input.LA(3);
				if ( (LA5_28=='e') ) {
					int LA5_33 = input.LA(4);
					if ( (LA5_33=='n') ) {
						int LA5_37 = input.LA(5);
						if ( ((LA5_37 >= '0' && LA5_37 <= '9')||(LA5_37 >= 'A' && LA5_37 <= 'Z')||LA5_37=='_'||(LA5_37 >= 'a' && LA5_37 <= 'z')) ) {
							alt5=6;
						}

						else {
							alt5=4;
						}

					}

					else {
						alt5=6;
					}

				}

				else {
					alt5=6;
				}

			}

			else {
				alt5=6;
			}

			}
			break;
		case 'e':
			{
			int LA5_5 = input.LA(2);
			if ( (LA5_5=='l') ) {
				int LA5_29 = input.LA(3);
				if ( (LA5_29=='s') ) {
					int LA5_34 = input.LA(4);
					if ( (LA5_34=='e') ) {
						int LA5_38 = input.LA(5);
						if ( ((LA5_38 >= '0' && LA5_38 <= '9')||(LA5_38 >= 'A' && LA5_38 <= 'Z')||LA5_38=='_'||(LA5_38 >= 'a' && LA5_38 <= 'z')) ) {
							alt5=6;
						}

						else {
							alt5=5;
						}

					}

					else {
						alt5=6;
					}

				}

				else {
					alt5=6;
				}

			}

			else {
				alt5=6;
			}

			}
			break;
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case '_':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'f':
		case 'g':
		case 'h':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'q':
		case 'r':
		case 's':
		case 'u':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
			{
			alt5=6;
			}
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			{
			alt5=7;
			}
			break;
		case '\n':
		case '\r':
			{
			alt5=8;
			}
			break;
		case '\t':
		case ' ':
			{
			alt5=9;
			}
			break;
		case '(':
			{
			alt5=10;
			}
			break;
		case ')':
			{
			alt5=11;
			}
			break;
		case '{':
			{
			alt5=12;
			}
			break;
		case '}':
			{
			alt5=13;
			}
			break;
		case '=':
			{
			alt5=14;
			}
			break;
		case '+':
			{
			alt5=15;
			}
			break;
		case '-':
			{
			alt5=16;
			}
			break;
		case '*':
			{
			alt5=17;
			}
			break;
		case '/':
			{
			alt5=18;
			}
			break;
		case '%':
			{
			alt5=19;
			}
			break;
		case '<':
			{
			alt5=20;
			}
			break;
		case '>':
			{
			alt5=21;
			}
			break;
		case '&':
			{
			alt5=22;
			}
			break;
		case '^':
			{
			alt5=23;
			}
			break;
		case '|':
			{
			alt5=24;
			}
			break;
		default:
			NoViableAltException nvae =
				new NoViableAltException("", 5, 0, input);
			throw nvae;
		}
		switch (alt5) {
			case 1 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:10: VAR
				{
				mVAR(); 

				}
				break;
			case 2 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:14: PRINT
				{
				mPRINT(); 

				}
				break;
			case 3 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:20: IF
				{
				mIF(); 

				}
				break;
			case 4 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:23: THEN
				{
				mTHEN(); 

				}
				break;
			case 5 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:28: ELSE
				{
				mELSE(); 

				}
				break;
			case 6 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:33: ID
				{
				mID(); 

				}
				break;
			case 7 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:36: INT
				{
				mINT(); 

				}
				break;
			case 8 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:40: NL
				{
				mNL(); 

				}
				break;
			case 9 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:43: WS
				{
				mWS(); 

				}
				break;
			case 10 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:46: LP
				{
				mLP(); 

				}
				break;
			case 11 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:49: RP
				{
				mRP(); 

				}
				break;
			case 12 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:52: LB
				{
				mLB(); 

				}
				break;
			case 13 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:55: RB
				{
				mRB(); 

				}
				break;
			case 14 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:58: PODST
				{
				mPODST(); 

				}
				break;
			case 15 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:64: PLUS
				{
				mPLUS(); 

				}
				break;
			case 16 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:69: MINUS
				{
				mMINUS(); 

				}
				break;
			case 17 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:75: MUL
				{
				mMUL(); 

				}
				break;
			case 18 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:79: DIV
				{
				mDIV(); 

				}
				break;
			case 19 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:83: MOD
				{
				mMOD(); 

				}
				break;
			case 20 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:87: LSHIFT
				{
				mLSHIFT(); 

				}
				break;
			case 21 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:94: RSHIFT
				{
				mRSHIFT(); 

				}
				break;
			case 22 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:101: BITAND
				{
				mBITAND(); 

				}
				break;
			case 23 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:108: BITXOR
				{
				mBITXOR(); 

				}
				break;
			case 24 :
				// /home/student/antlr_swing_forked/antlr_swing/src/tb/antlr/Expr.g:1:115: BITOR
				{
				mBITOR(); 

				}
				break;

		}
	}



}
