tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=block | e+=expr |  e+=assign | e+=ifstmt | d+=decl)* -> program(name={$e},deklaracje={$d})
    ;
    
block
@init {
       symbols.enterScope();
}
@after {
       symbols.leaveScope();
}
      : ^(BLOCK (e+=block | e+=expr | d+=decl)*) -> zakres(stmts={$e}, decls={$d})
      ;

decl  :
        ^(VAR i1=ID) {symbols.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
    
ifstmt  : ^(IF e1=expr e2=expr e3=expr?) {numer++;} -> if(e1={$e1.st}, e2={$e2.st}, e3={$e3.st}, ref={numer.toString()})
        ;
        
assign : ^(PODST i1=ID   e2=expr) -> setVar(n={$i1.text},val={$e2.st})
      ;

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subtract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st}) 
        | ^(NEQ e1=expr e2=expr)   -> notEqual(p1={$e1.st},p2={$e2.st})
        | ^(EQ e1=expr e2=expr) {numer++;}   ->  equal(p1={$e1.st},p2={$e2.st}, ref={numer.toString()})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ID                       -> id(n={$ID.text})
    ;
    