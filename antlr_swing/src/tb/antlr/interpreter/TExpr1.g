tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (expr | declAssignVar | print | block)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(RSHIFT   e1=expr e2=expr) {$out = $e1.out >> $e2.out;}
        | ^(LSHIFT   e1=expr e2=expr) {$out = $e1.out << $e2.out;}
        | ^(BITAND   e1=expr e2=expr) {$out = $e1.out & $e2.out;}
        | ^(BITXOR   e1=expr e2=expr) {$out = $e1.out ^ $e2.out;}
        | ^(BITOR  e1=expr e2=expr) {$out = $e1.out | $e2.out;}
        | INT                       {$out = getInt($INT.text);}
        | ID                        {$out = symbolsMemory.getSymbol($ID.text);}
        ;
        catch [RuntimeException ex] {
          drukujBlad(ex.toString());
        }
        
declAssignVar
        : ^(VAR i1=ID)              {symbolsMemory.newSymbol($i1.text);}
        | ^(PODST i1=ID   e2=expr)  {symbolsMemory.setSymbol($i1.text, $e2.out);}
        ;
        catch [RuntimeException ex] {
          drukujBlad(ex.toString());
        }
        
print   : ^(PRINT e1=expr)          {drukuj($e1.text, $e1.out);}
        ;

block  
@init {
       symbolsMemory.enterScope();
}
@after {
        symbolsMemory.leaveScope();
}
        : ^(BLOCK (expr | declAssignVar | print | block)*)
        ;
        catch [RuntimeException ex] {
          drukujBlad(ex.toString());
        } 