package tb.antlr.interpreter;

@SuppressWarnings("serial")
public class ZeroDivisionException extends RuntimeException {
	ZeroDivisionException() {
		super("W wyrazeniu wystapilo dzielenie przez 0.");
	}
}
