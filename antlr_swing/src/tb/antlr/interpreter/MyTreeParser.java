package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }
    
    protected LocalSymbols symbolsMemory = new LocalSymbols();
    
    protected int divide(int v1, int v2) {
    	if(v2 == 0) {
    		throw new ZeroDivisionException();
    	}
    	else {
    		return v1 / v2;
    	}
    }

    protected void drukuj(String text, Object result) {
    	if(result == null) {
    		System.out.println("W wypisywanym wyrazeniu wystapil blad i ma ono pusta wartosc!");
    	}
    	else {
    		text = text + " = " + result.toString();
    		System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    	}
    }
    
    protected void drukujBlad(String text) {
    	System.out.println(text);
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
