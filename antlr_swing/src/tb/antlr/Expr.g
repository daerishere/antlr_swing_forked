grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

tokens {
  BLOCK;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : lines EOF!
    ;
    
lines
    : (stat | block)+
    ;
        
block
    : LB body=lines RB NL -> ^(BLOCK $body) 
    ;

stat
    : expr NL -> expr
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | IF cond=expr THEN th=expr (ELSE el=expr)? NL -> ^(IF $cond $th $el?)
    | NL ->
    ;
    
expr
  : bitxorexpr
    ( BITOR^ bitxorexpr
    )*
  ;

bitxorexpr
  : bitandexpr
    ( BITXOR^ bitandexpr
    )*
  ;
  
bitandexpr
  : equalityexpr
    ( BITAND^ equalityexpr
    )*
  ;
  
 equalityexpr
  : shiftexpr
    ( EQ^ shiftexpr
    | NEQ^ shiftexpr
    )*
    ;

shiftexpr
  : addExpr
    ( LSHIFT^ addExpr
    | RSHIFT^ addExpr
    )*
  ; 

addExpr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

PRINT : 'print';

IF : 'if';

THEN : 'then';

ELSE : 'else';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;
	
LB
  : '{'
  ;
  
RB
  : '}'
  ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

MOD
  : '%'
  ;
  
 LSHIFT
  : '<<'
  ;
  
RSHIFT
  : '>>'
  ;
  
BITAND 
  : '&'
  ;
  
BITXOR 
  : '^'
  ;

BITOR
  : '|'
  ;
  
EQ
  : '=='
  ;
  
NEQ
  : '!='
  ;
